<?php

/**
 * @file
 * Provide field administration routines.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Implements hook_field_instance_settings_form().
 */
function _county_field_instance_settings_form($field, $instance) {
  $settings = $instance['settings'];

  $bundle_instances = field_info_instances($instance['entity_type'], $instance['bundle']);
  unset($bundle_instances[$instance['field_name']]);

  $supported_field_types = array('text', 'list_text');
  $potential_fields = array();

  // Get a list of fields on this entity that are of a supported type.
  foreach ($bundle_instances as $bundle_instance) {
    $potential_field = field_info_field($bundle_instance['field_name']);
    if (in_array($potential_field['type'], $supported_field_types)) {
      $potential_fields[$bundle_instance['field_name']] = $bundle_instance['label'];
    }
  }

  foreach (array('default', 'short', 'key') as $key) {
    $display_types[$key] = t(ucfirst($key));
  }

  // @todo Add a hidden country field so this is ready to handle all tuples of
  // (country, admin_area, sub admin_area).
  $form['admin_area_field'] = array(
    '#type' => 'select',
    '#title' => t('State field'),
    '#default_value' => isset($settings['admin_area_field']) ? $settings['admin_area_field']: '',
    '#options' => $potential_fields,
    '#description' => t('Select the state field used to filter the county list.'),
    '#required' => TRUE,
  );
  $form['display_type'] = array(
    '#type' => 'select',
    '#title' => t('Display type'),
    '#default_value' => isset($settings['display_type']) ? $settings['display_type']: '',
    '#options' => $display_types,
    '#description' => t('Select the type of value to display on edit form.'),
    '#required' => TRUE,
  );
  $form['append_fips'] = array(
    '#type' => 'checkbox',
    '#title' => t('Append FIPS code'),
    '#default_value' => isset($settings['append_fips']) ? $settings['append_fips']: '',
    '#description' => t('If checked, then append FIPS code to display value.'),
    '#element_validate' => array('county_instance_append_fips_validate'),
    '#states' => array(
      'visible' => array(
        ':input[name="instance[settings][display_type]"]' => array('!value' => 'key'),
      ),
    ),
  );
  // @todo Add a validation handler and require cardinality = 1 on this field
  // and the admin_area_field?
  $form['select_all'] = array(
    '#type' => 'checkbox',
    '#title' => t('Select all'),
    '#default_value' => isset($settings['select_all']) ? $settings['select_all']: '',
    '#description' => t('Add option to select all counties to the county list.'),
  );

  return $form;
}

/**
 * Form element validation handler for append_fips element.
 */
function county_instance_append_fips_validate($element, &$form_state) {
  // Ensure append_fips is false if display_type is key.
  $values = &$form_state['values']['instance']['settings'];
  $values['append_fips'] = $values['display_type'] == 'key' ? 0 : $values['append_fips'];
}

/**
 * Implements hook_field_formatter_settings_form().
 *
 * When called from views_ui the $instance has no settings from the field in
 * question but only the default values. The instance has been associated with
 * ctools instead of the actual entity_type and bundle.
 *
 * views_handler_field_field.inc: options_form()
 *   $this->instance = ctools_fields_fake_field_instance($this->definition['field_name'], '_custom', $formatter, $settings);
 */
function _county_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  // Gather data.
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $form_state['field_name'] = $field_name = $instance['field_name'];
  $type = '';
  if (isset($form_state['values']['fields'][$field_name])) {
    // The form is rendered after submit of field_ui_display_overview_form().
    $type = $form_state['values']['fields'][$field_name]['type'];
  }
  elseif (isset($form_state['view']) && is_object($form_state['view'])) {
    // The form is rendered in views UI.
    $handler = $form_state['view']->display_handler->get_handler('field', $field_name);
    $type = $handler->options['type'];
  }

  // Ensure validation handlers are loaded when form is in play.
  form_load_include($form_state, 'inc', 'county', 'includes/admin');

  // Build form elements.
  $element['display_all_text'] = array(
    '#type' => 'checkbox',
    '#title' => t('Replace all values'),
    '#description' => t('If field value includes all values for the administrative area, then replace list of individual values with the text "All counties".'),
    '#default_value' => $settings['display_all_text'],
  );

  $element['display_all_except'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display missing values'),
    '#description' => t('Display all values for the administrative area not included in the field value. Text will read "All counties except ...".<br />This setting only applies if the number of missing values is less than the number of values.<br />If no missing values, then text will read "All counties".<br />The exception list will be limited to number of values indicated below.'),
    '#default_value' => $settings['display_all_except'],
  );

  $element['limit_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of values'),
    '#description' => t('Limit display to this number of values based on alphabetical sort order. If zero, then display all values.'),
    '#default_value' => $settings['limit_count'],
    '#size' => 5,
  );

  if ($type != 'county_key') {
    $element['append_fips'] = array(
      '#type' => 'checkbox',
      '#title' => t('Append FIPS code'),
      '#description' => t('If checked, then append FIPS code to display value.'),
      '#default_value' => isset($settings['append_fips']) ? $settings['append_fips']: '',
      '#element_validate' => array('county_formatter_append_fips_validate'),
    );
  }

  return $element;
}

/**
 * Form element validation handler for append_fips element.
 */
function county_formatter_append_fips_validate($element, &$form_state) {
  $type = '';
  $values = [];
  if (isset($form_state['values']['fields'])) {
    $field_name = $form_state['field_name'];
    $type = $form_state['values']['fields'][$field_name]['type'];
    $values = &$form_state['values']['fields'][$field_name]['settings_edit_form']['settings'];
  }
  elseif (isset($form_state['values']['options'])) {
    $type = $form_state['values']['options']['type'];
    $values = &$form_state['values']['options']['settings'];
  }

  if ($type == 'county_key') {
    // Ensure append_fips is false if formatter is county_key.
    $values['append_fips'] = 0;
  }
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function _county_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  // Allow for formatters that exist before this setting is added.
  $settings += array(
    'display_all_text' => TRUE,
    'display_all_except' => FALSE,
    'limit_count' => 0,
    'append_fips' => FALSE
  );

  $summary = array();
  $summary[] = $settings['display_all_text'] ? t('Replace all values with "All counties".') : NULL;
  $summary[] = $settings['display_all_except'] ? t('Display "All counties except &lt;list&gt;" instead of each value.') : NULL;
  $summary[] = $settings['limit_count'] ? t('Limit display to first @limit_count values based on alpha order.', ['@limit_count' => $settings['limit_count']]) : NULL;
  $summary[] = $settings['append_fips'] ? t('Append FIPS code to display value.') : NULL;
  $summary = array_values(array_filter($summary));
  $summary += array(t('Display each value.'));

  return implode('<br />', $summary);
}
