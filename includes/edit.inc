<?php

/**
 * @file
 * Provide field edit routines.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Implements hook_field_widget_form().
 */
function _county_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  // Ensure validation handlers are loaded when form is in play.
  form_load_include($form_state, 'inc', 'county', 'includes/edit');

  switch ($instance['widget']['type']) {
    case 'county_select':
      // The parameters to hook_options_list(,,,$entity) include the storage
      // value (in $entity) but not the current form value (in $element); store
      // the options on the entity.
      $admin_area = county_admin_value($form, $form_state, $instance, $langcode, $element);
      // The '#entity' key is NULL on the field ui edit form.
      $element['#entity'] = empty($element['#entity']) ? new stdClass() : $element['#entity'];
      $element['#entity']->county_field_widget_options = _county_widget_options($admin_area, $instance['settings']);

      // Switch the widget type; build the form element.
      $instance['widget']['type'] = 'options_select';
      $element = options_field_widget_form($form, $form_state, $field, $instance, $langcode, $items, $delta, $element);
      // Display up to 12 rows, not the 5 used by options module.
      $element['#size'] = $element['#multiple'] ? min(12, count($element['#options'])) : 0;
      array_unshift($element['#element_validate'], 'county_field_widget_validate');

      // Store id for later use on the wrapper element.
      $parents_key = implode('-', $element['#field_parents']);
      $county_field_name = $instance['field_name'];
      $element['#county_id'] = $id = county_wrapper_id($parents_key, $county_field_name);

      // To handle multiple county elements on a form (but not necessarily
      // across multiple forms on a page request), tied to the same or different
      // admin_area elements, store an array keyed by the admin_area_field_name
      // so that county_widget_refresh() can loop on the county fields tied to
      // the triggering admin_area element.
      $settings = $instance['settings'];
      $admin_area_field_name = $settings['admin_area_field'];
      $langcode = empty($langcode) ? LANGUAGE_NONE : $langcode;
      $form_state['county_field'][$parents_key][$admin_area_field_name][$county_field_name] = $langcode;
/*
      $options = county_widget_options($admin_area);
      $default_value = is_array($items) && isset($items[$delta]) ? array_shift($items[$delta]) : '_none';
      $default_value = isset($options[$default_value]) ? $default_value : '_none';
      $cardinality = isset($instance['cardinality']) ? $instance['cardinality'] : $field['cardinality'];
      $multiple = $cardinality != 1;

      $element += array(
        '#type' => 'select',
        '#default_value' => $default_value,
        '#options' => $options,
        '#multiple' => $multiple && count($options) > 1,
        '#size' => $multiple ? min(12, count($options)) : 0,
      );
      if ($multiple) {
        $extra = array(
          '#markup' => field_filter_xss('Use Ctrl+Click to select or deselect multiple items.'),
        );
      }
*/
      break;
  }

  return $element;

  // If 'multiple values' => FIELD_BEHAVIOR_DEFAULT, then form API expects the
  // widget form to return an array keyed by columns in the field storage. Hence
  // something like return array('value' => $element). Field API adds this to an
  // array keyed on $delta so the form structure matches the $items structure.
  // If 'multiple values' => FIELD_BEHAVIOR_CUSTOM, then the field module is on
  // its own. The options module is one such module; it ends the form array
  // structure at the langcode key.
  //
  // To piggyback on the pattern of the options module, the 'value' key should
  // be omitted. This is tied up in the calls to _options_form_to_storage and
  // form_set_value. With the 'value' key in $element, the array in $form_state
  // has the 'value' key between langcode and delta portion of the $items
  // structure, like ['county']['und']['value'][0]['value'] => 01103. At the end
  // of the day, the submitted values are not saved to the storage.

  return isset($extra) ? array('value' => $element, 'instructions' => $extra) : array('value' => $element);
}

/**
 * Form element validation handler for county element.
 *
 * @see options_field_widget_validate()
 */
function county_field_widget_validate($element, &$form_state) {
  if ($element['#multiple'] && is_array($element['#value']) /*&& count($element['#value']) == 1*/) {
    if (count($element['#value']) == 1) {
      // This manipulation is needed because this module always adds '_none' item.
      // The options module only adds this item when multiple and not required, in
      // which case it does not care whether '_none' is selected and removes it in
      // _options_form_to_storage().
      $element['#value'] = current($element['#value']);
      if ($element['#required'] && $element['#value'] == '_none') {
        form_error($element, t('!name field is required.', array('!name' => $element['#title'])));
      }
    }
    elseif (in_array('all', $element['#value'])) {
      form_error($element, t('!name field may either be all counties or individual counties.', array('!name' => $element['#title'])));
    }
  }
}

/**
 * Implements hook_field_widget_error().
 */
function county_field_widget_error($element, $error, $form, &$form_state) {
  form_error($element, $error['message']);
}

/**
 * Implements hook_field_validate().
 *
 * Possible error codes:
 * - 'county_illegal_value': The value is not a valid US county FIPS code.
 */
function county_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  foreach ($items as $delta => $item) {
    if (!empty($item['value']) && $item['value'] != '_none') {
      // Alright to run this query as $delta should always equal one.
      // @todo This condition is no longer possible.
      if ($item['value'] == '_none') {
        $errors[$field['field_name']][$langcode][$delta][] = array(
          'error' => 'county_illegal_value',
          'message' => t('%name: illegal value.', array('%name' => $instance['label'])),
        );
      }
    }
  }
}

/**
 * Implements hook_field_attach_form().
 *
 * This does the following:
 * - sets a fixed ID on the county field wrapper DIV (needed for AJAX callback)
 * - adds AJAX handling to a change of the administrative area field
 *
 * Prefer this hook to:
 *   hook_form_alter() as it is called:
 *   - only in the context of a form with a field
 *   an after_build callback as it is called:
 *   - before the form is cached (this is the retrieved form)
 *   - as drupal_process_form() caches the $unprocessed_form
 *   - so the '#ajax' stuff is present when called by ajax_form_callback()
 *
 * @see field_attach_form()
 */
function _county_field_attach_form($entity_type, $entity, &$form, &$form_state, $langcode) {
  if (empty($form_state['county_field'])) {
    return;
  }

  foreach ($form_state['county_field'] as $parents_key => $admin_area_field_names) {
    foreach ($admin_area_field_names as $admin_area_field_name => $county_field_names) {
      foreach ($county_field_names as $county_field_name => $langcode) {
        if (empty($form[$county_field_name])) {
          continue;
        }
        $element = &$form[$county_field_name];
        if (!empty($element['#county_processed'])) {
          continue;
        }
        $element['#county_processed'] = 1;
        // Fix the HTML ID for the Ajax callback.
        // @todo Append the entity ID in case of multiple forms on page.
        $element['#id'] = $element[$langcode]['#county_id'];
      }

      // @todo Is this element always present at this key?
      if (!isset($form[$admin_area_field_name])) {
        // This hook may be invoked more than once on a form where $form is only a
        // subset of the entire form and omits the admin_area_field_name, but
        // $form_state includes the 'county_field' value. Example: profile2.
        continue;
      }
      $admin_area_element = &$form[$admin_area_field_name];
      if (!empty($admin_area_element['#county_processed'])) {
        continue;
      }
      $admin_area_element['#county_processed'] = 1;
      $langcode = empty($admin_area_element['#language']) ? LANGUAGE_NONE : $admin_area_element['#language'];

      $parents = $form['#parents'];
      $ajax = array(
        // Only validate the administrative area field (the only dependent field).
        // This eventually makes it the only value in $form_state['values'].
//         '#limit_validation_errors' => array(array_merge($parents, array($admin_area_field_name, $langcode))),
        '#limit_validation_errors' => array(
          array_merge($parents, array($admin_area_field_name, $langcode)),
          // Do not include the county field; an error will occur if it is required
          // since it will have no value or an illegal value.
//           array_merge($parents, array($county_field_name, $langcode)),
        ),
        '#ajax' => array(
          'callback' => 'county_widget_refresh',
          'effect' => 'fade', // @todo Add this to $settings in ajax_command_replace()?
          'event' => 'change',
        ),
        // Add key for multiple value form not otherwise present at this depth.
        '#field_name' => $admin_area_field_name,
      );

      // Field API goes deep with the array keys when there are parents.
      $field_state = field_form_get_state($parents, $admin_area_field_name, $langcode, $form_state);
      $admin_area_field_type = $field_state['field']['type'];
      if ($admin_area_field_type == 'list_text') {
        // A list_text field uses the options module for its widgets; the latter sets
        // ['behaviors']['multiple values'] = FIELD_BEHAVIOR_CUSTOM. This property is
        // checked in field_default_form() to determine whether to call
        // field_multiple_value_form() and include a delta key in the form element.
        // Because of this property, a list_text field does not have a delta key in
        // its field form element.
        // See options_field_widget_info().

        // Check for a delta key before adding this ajax item.
        $has_delta_key = isset($admin_area_element[$langcode]['#theme']) && $admin_area_element[$langcode]['#theme'] == 'field_multiple_value_form';
        if ($has_delta_key) {
          $admin_area_element[$langcode][0] += $ajax;
        }
        else {
          $admin_area_element[$langcode] += $ajax;
        }
      }
      else {
        // A simple text field has a 'value' key which has the '#type' property
        // and needs to have the '#ajax' property so ajax_pre_render_element()
        // will process the element.
        // @todo If we are to support other field types, then we may need to look
        // at the field columns and use the first column in the array.
        $admin_area_element[$langcode][0]['value'] += $ajax;
      }
    }
  }
}

/**
 * Returns CSS ID for county wrapper element.
 *
 * @param string $parents_key
 *   The array key based on parents array.
 * @param string $county_field_name
 *   The county field name.
 *
 * @return string
 *   The CSS ID for county wrapper element.
 */
function county_wrapper_id($parents_key, $county_field_name) {
  // Allow for $parents_key to be blank and omit it from $parts.
  $parts = array_filter(['edit', $parents_key, $county_field_name, 'wrapper']);
  return implode('-', $parts);
}

/**
 * Returns the administrative area value.
 *
 * This is either the last submitted value, the entity value, or NULL.
 *
 * @return string
 *   The administrative area value.
 */
function county_admin_value($form, $form_state, $instance, $langcode, $element) {
  $settings = $instance['settings'];
//   $county_field_name = $form_state['county_field'];
  $admin_area_field_name = $settings['admin_area_field'];

  $key_exists = NULL;
  $values = NULL;

  if (!empty($form_state['input'])) {
    // Start with submitted values.
    // @see field_default_extract_form_values()

    // Input values for the entire form are always in $form_state['input']
    // except for disbled elements. This is not true of $form_state['values']
    // if the triggering element has a '#limit_validation_errors' property that
    // excludes some of the county elements. This is likely to occur with
    // multiple county elements on a form, or multiple elements with an AJAX
    // callback.

    // For a list_text field, the 'inputs' array ends at the 'langcode' key so
    // omit it to ensure an array for consistency and as expected by code
    // follwing this block.
    $path = array_merge($form['#parents'], array($admin_area_field_name, /*$langcode*/));
    $values = drupal_array_get_nested_value($form_state['input'], $path, $key_exists);
  }

  if (!$key_exists && isset($element['#entity'])) {
    // Fall back to entity values.
    // If adding an entity, then $values is FALSE as no fields are present.
    $entity_type = $element['#entity_type'];
    $entity = $element['#entity'];
    $values = field_get_items($entity_type, $entity, $admin_area_field_name, $langcode);
  }

  // Shift array values until we end up with a string. Depending on the field
  // type and cardinality, this implies 1-3 shift operations.
  // Array for list_text is [field_name][langcode].
  // Array for text is [field_name][langcode][delta][column, {_weight}].
  // A _weight key is present in delta array if cardinality > 1.
  $values = is_array($values) ? array_shift($values) : '_none';
  $value = is_array($values) ? array_shift($values) : $values;
  $value = is_array($value) ? array_shift($value) : $value;

  return $value;
}

/**
 * AJAX callback: Change of administrative area field.
 */
function county_widget_refresh($form, $form_state) {
  $selector = '';
  $commands = [];
  $trigger = $form_state['triggering_element'];
  $admin_area_field_name = isset($trigger['#field_name']) ? $trigger['#field_name'] : 'MISSING';
  $parents = $trigger['#parents'];

  // Find parents of the admin_area_field.
  $key = array_search($admin_area_field_name, $parents);
  $parents = $key ? array_slice($parents, 0, $key) : [];
  $parents_key = implode('-', $parents);

  foreach ($form_state['county_field'][$parents_key][$admin_area_field_name] as $county_field_name => $langcode) {
    if (!empty($form[$county_field_name])) {
      $county_element = $form[$county_field_name];
    }
    else {
      // The county element is nested in form array; find the parent keys.
      $field_state = field_form_get_state($parents, $county_field_name, $langcode, $form_state);
      $parents = $field_state['array_parents'];

      // Remove the langcode key.
      $parents = array_slice($parents, 0, -1);
      $county_element = drupal_array_get_nested_value($form, $parents, $key_exists);
      if (!$key_exists) {
        // @todo Do something more in this case.
        return [];
      }
    }
    // Remove the '_weight' element inserted by the field API if cardinality != 1.
    unset($county_element['_weight']);
    $html = drupal_render($county_element);
    if ($html) {
      $selector = '#' . county_wrapper_id($parents_key, $county_field_name);
      $commands[] = ajax_command_replace($selector, $html);
    }
  }
  if ($selector) {
    // This prepends the messages to the last selector.
    $commands[] = ajax_command_prepend($selector, theme('status_messages'));
  }
  return ['#type' => 'ajax', '#commands' => $commands];
}

/**
 * Returns a county options list suitable for a select element.
 *
 * @param string $admin_area
 *   The administrative area value.
 * @param array $settings
 *   (optional) Associative array with keys:
 *   'display_type': The value to display for the county:
 *   - 'default': full county name
 *   - 'short': the county name without the suffix.
 *   - 'key': the county FIPS value.
 *   'append_fips': Whether to append the FIPS value.
 *   'select_all': Whether to add an 'All counties' option.
 *
 * @return array
 *   The county list corresponding to the administrative area.
 */
function _county_widget_options($admin_area, $settings = array()) {
  // See _options_get_options().
  $temp['widget']['type'] = 'options_select';
  $label = theme('options_none', array('instance' => $temp, 'option' => 'option_none'));
  $options = $none = array('_none' => $label);

  if ($admin_area == '_none') {
    // @todo Check the '#empty_value' property of the administrative area field.
    return $none;
  }

  $settings += array(
    'display_type' => 'default',
    'append_fips' => FALSE,
    'select_all' => FALSE
  );
  extract($settings);
  $select_all ? $options['all'] = 'All counties' : '';

  $args = array(':state' => $admin_area);
  $options += db_query('SELECT CONCAT(fips_state, fips_county) AS fips, county FROM {county_fips} WHERE state = :state', $args)->fetchAllKeyed();
  // @todo Use empty(array_diff_key()) with PHP 5.5.
  if (array_diff_key($options, array_flip(array('_none', 'all'))) == array()) {
    return $none;
  }

  if ($display_type == 'short') {
    // @todo Consider retrieving the fips_code and using it as basis to remove
    // 'county' from string. Or making this a widget setting.
    // @todo Sort the suffix strings: county is most likely. Use a replace
    // callback and return if a suffix is found.
    $suffixes = ['County', 'counties', 'Parish', 'City and Borough', 'Borough', 'Census Area', 'city', 'District', 'Municipality', 'Municipio', 'Island'];
    foreach ($options as $key => &$value) {
      $value = trim(str_replace($suffixes, '', $value));
    }
  }
  elseif ($display_type == 'key') {
    foreach ($options as $key => &$value) {
      $value = $key;
    }
  }
  if ($append_fips) {
    foreach ($options as $key => &$value) {
      if ($key != '_none' && $key != 'all') {
        $value .= " ($key)";
      }
    }
  }

  return $options;
}
