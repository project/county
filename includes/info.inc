<?php

/**
 * @file
 * Provides info-type hook implementations that are infrequently called.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Implements hook_help().
 */
function _county_help($path, $arg) {
  switch ($path) {
    case 'admin/help#county':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The County module defines a field for storing the US county associated with an US state. See the <a href="@field-help">Field module help page</a> for more information about fields.', array('@field-help' => url('admin/help/field'))) . '</p>';
      return $output;
  }
}

/**
 * Implements hook_field_info().
 */
function _county_field_info() {
  return array(
    'county' => array(
      'label' => t('County'),
      'description' => t('This field stores an US county FIPS code as varchar text in the database.'),
      'settings' => array(),
      'instance_settings' => array(
        'admin_area_field' => '',
        'display_type' => 'default',
        'append_fips' => FALSE,
        'select_all' => FALSE
      ),
      'default_widget' => 'county_select',
      'default_formatter' => 'county_default',
      'property_type' => 'text',
    ),
  );
}

/**
 * Implements hook_field_formatter_info().
 */
function _county_field_formatter_info() {
  $base = array(
    'field types' => array('county'),
    'settings' => array(
      'display_all_text' => TRUE,
      'display_all_except' => FALSE,
      'limit_count' => 0,
      'append_fips' => FALSE,
    ),
  );
  return array(
    'county_default' => array(
      'label' => t('Default'),
      'description' => t('County name'),
    ) + $base,
    'county_short' => array(
      'label' => t('Short'),
      'description' => t('County name without suffix'),
    ) + $base,
    'county_key' => array(
      'label' => t('Key'),
      'description' => t('County FIPS code'),
    ) + $base,
  );

}

/**
 * Implements hook_field_widget_info().
 */
function _county_field_widget_info() {
  return array(
    'county_select' => array(
      'label' => t('Select list'),
      'field types' => array('county'),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
      ),
      'settings' => array(),
    ),
  );
}
