<?php

/**
 * @file
 * Provide field view routines.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Implements hook_field_formatter_view().
 */
function _county_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  if (empty($items)) {
    return $element;
  }

  $settings = $instance['settings'];
  if (empty($settings['admin_area_field'])) {
    return $element;
  }
  $admin_area_field = $settings['admin_area_field'];
  if (empty($entity->{$admin_area_field}[$langcode])) {
    return $element;
  }

  $admin_area = $entity->{$admin_area_field}[$langcode][0]['value'];
  $options = county_display_value($admin_area);

  $display_missing = $display_limit = FALSE;
  $settings = $display['settings'];
  // Allow for formatters that exist before this setting is added.
  $settings += array(
    'display_all_text' => TRUE,
    'display_all_except' => FALSE,
    'limit_count' => 0,
    'append_fips' => FALSE
  );
  if (!empty($settings['display_all_text']) || !empty($settings['display_all_except'])) {
    $items_transposed = options_array_transpose($items);
    $items_transposed = array_flip($items_transposed['value']) + array_flip(array('_none', 'all'));
    $diff = array_diff_key($options, $items_transposed);
    if (!empty($settings['display_all_text'])) {
      if (empty($diff)) {
        $element[0] = array('#markup' => t('All counties'));
        return $element;
      }
    }
    if (!empty($settings['display_all_except'])) {
      if (count($items) > count($diff)) {
        // Display the missing values.
        $display_missing = TRUE;
        // @todo Until county_display_value() is changed use keys of $diff.
//         $items = $diff;
        $items = options_array_transpose(array('value' => array_keys($diff)));
      }
    }
  }

  $count = $limit = 0;
  if (!empty($settings['limit_count'])) {
    $limit = (int) $settings['limit_count'];
    if ($limit && $limit < count($items)) {
      $display_limit = TRUE;
      $count = count($items);
      $items = array_slice($items, 0, $limit);
    }
  }

  switch ($display['type']) {
    case 'county_default':
      foreach ($items as $delta => $item) {
        $extra = $settings['append_fips'] && !in_array($delta, array('_none', 'all'), TRUE) ? " ({$item['value']})" : '';
        $output = field_filter_xss(county_display_value($admin_area, $item['value']) . $extra);
        $element[$delta] = array('#markup' => $output);
      }
      break;

    case 'county_short':
      // @todo Sort the suffix strings: county is most likely. Use a replace
      // callback and return if a suffix is found.
      $suffixes = ['County', 'counties', 'Parish', 'City and Borough', 'Borough', 'Census Area', 'city', 'District', 'Municipality', 'Municipio', 'Island'];
      foreach ($items as $delta => $item) {
        $extra = $settings['append_fips'] && !in_array($delta, array('_none', 'all'), TRUE) ? " ({$item['value']})" : '';
        $output = trim(str_replace($suffixes, '', county_display_value($admin_area, $item['value'])));
        $output = field_filter_xss($output . $extra);
        $element[$delta] = array('#markup' => $output);
      }
      break;

    case 'county_key':
      foreach ($items as $delta => $item) {
        $element[$delta] = array('#markup' => field_filter_xss($item['value']));
      }
      break;
  }

  // @todo The next two items do not work well in a view set to display all
  // county values on one line with a simple separator (including a comma or
  // similar character). The separator added before the first item and after
  // the last makes for incorrect punctuation. Try to detect whether this is a
  // view with that setting and concatenate these strings to the firs and last
  // items, respectively.
  if ($display_missing) {
    $begin = array('#markup' => count($diff) ? t('All counties except') : t('All counties'));
    array_unshift($element, $begin);
  }
  if ($display_limit) {
    $element[] = array('#markup' => t('(first @limit of @count values)', array('@limit' => $limit, '@count' => $count)));
  }

  return $element;
}

/**
 * Returns an associative array of county display values or one display value.
 *
 * @param string $admin_area
 *   The administrative area value.
 * @param string $county
 *   (optional) The county FIPS value.
 *
 * @return array|string
 *   An associative array of county display values or one display value.
 */
function county_display_value($admin_area, $county = NULL) {
  $values = &drupal_static(__FUNCTION__, array());

  if (!isset($values[$admin_area])) {
    // Retrieve values from database.
    $args = array(':state' => $admin_area);
    $options = db_query('SELECT CONCAT(fips_state, fips_county) AS fips, county FROM {county_fips} WHERE state = :state', $args)->fetchAllKeyed();
    if (!$options) {
      $options = array();
    }
    // Add default values.
    $options['_none'] = 'None'; // Need this?
    $options['all'] = 'All counties';
    $values[$admin_area] = $options;
    // No harm to set this multiple times (potentially but not likely).
    $values['_none'] = array('_none' => 'None');
  }

  if (isset($county)) {
    return isset($values[$admin_area][$county]) ? $values[$admin_area][$county] : 'None';
  }
  return $values[$admin_area];
}
