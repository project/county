<?php

/**
 * @file
 * Contains \county_handler_filter_many.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Filters on the county FIPS value while displaying county text in select.
 *
 * @todo Filter by FIPS value. Might also allow syntax like 'KS:Riley'.
 */
class county_handler_filter_many extends views_handler_filter_many_to_one {

  /**
   * Adds filter to the query.
   */
  function query__SAVE() {
    // @todo A join would be simpler if the county_fips table included a column
    // with same contents as the value column in a field table.
    $this->ensure_my_table();
    $field = "$this->table_alias.$this->real_field";
    $this->sub_select($field);
  }

  // Compare filter value to 'county' column in county_fips table.
  function sub_select($field) {
    $value = db_like($this->value);
    $sql = "$field IN (SELECT CONCAT(fips_state, fips_county) FROM county_fips WHERE county LIKE '%$value%')";
    $this->query->add_where_expression($this->options['group'], $sql);
  }

  public $value_form_type = 'select';

  function option_definition() {
    $options = parent::option_definition();
    $options['admin_area_argument'] = array('default' => '');
    $options['admin_area_filter'] = array('default' => '');
    return $options;
  }

  /**
   * Form builder.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Gather data.
    // Create an array of configured filters.
    $argument_fields = $this->options_list('argument');
    $filter_fields = $this->options_list('filter');

    // Build form elements.
    // Allow for an argument [contextual filter] and a filter as basis for the
    // admin_area_field. Indicate which is preferred.
    $form['admin_area_argument'] = array(
      '#type' => 'select',
      '#title' => t('State field [argument]'),
      '#default_value' => $this->options['admin_area_argument'],
      '#options' => $argument_fields,
      '#empty_value' => '_none',
      '#empty_option' => '- None -',
      '#description' => t('Select the state argument used to filter the county list.'),
    );
    $form['admin_area_filter'] = array(
      '#type' => 'select',
      '#title' => t('State field [filter]'),
      '#default_value' => $this->options['admin_area_filter'],
      '#options' => $filter_fields,
      '#empty_value' => '_none',
      '#empty_option' => '- None -',
      '#description' => t('Select the state filter used to filter the county list.'),
    );
  }

  /**
   * Returns associative array of fields by handler type.
   */
  function options_list($type) {
    $options = [];
    foreach ($this->view->display_handler->get_handlers($type) as $id => $handler) {
      if ($id == $this->options['id']) {
        continue;
      }
      $options[$id] = isset($handler->definition['title short']) ? $handler->definition['title short'] : $handler->definition['title'];
    }
    return $options;
  }

  /**
   * Adds filter to the query.
   */
  function query() {
    if (is_array($this->value) && in_array('all', $this->value)) {
      // All counties are allowed; no query additions needed.
      return;
    }
    parent::query();
  }

  /**
   * Form builder.
   *
   * Allow for an argument [contextual filter] and a filter as basis for the
   * admin_area_field. Indicate which is preferred.
   *
   * If admin_area_field comes from an exposed filter, then would like to do the
   * usual county widget form changes with AJAX when building the exposed form
   * element, but can not as this form uses a GET method and core AJAX only
   * works with a POST method.
   *
   * @see views_handler_filter_in_operator::value_form().
   */
  function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);

    require_once __DIR__ . '/../../includes/edit.inc';
    $this->admin_area_settings($form, $form_state);
    if ($this->admin_area_source == 'filter') {
      $form['#parents'] = [];
      $element = &$form['value'];

      // Copied from county_field_widget_form().
      $parents_key = '';
      $county_field_name = $this->options['field'];
      $element['#county_id'] = $id = county_wrapper_id($parents_key, $county_field_name);

      $admin_area_field_name = $this->admin_area_field;
      $langcode = empty($langcode) ? LANGUAGE_NONE : $langcode;
      $form_state['county_field'][$parents_key][$admin_area_field_name][$county_field_name] = $langcode;

      // OMIT: views exposed form uses 'GET' method, core ajax requires 'POST'.
      return;

      // Attach AJAX to state element change.

/*
      $entity_type = $entity = '';
      $langcode = LANGUAGE_NONE;
      county_field_attach_form($entity_type, $entity, $form, $form_state, $langcode);
*/
      $langcode = LANGUAGE_NONE;

      // Copied from county_field_attach_form().
      // @see views_form_views_exposed_form_alter()
      // The above sets access = false on the form_build_id which is required
      // by regular core ajax processing.
//       $element = &$form[$county_field_name];
      if (!empty($element['#county_processed'])) {
        return;
      }
      $element['#county_processed'] = 1;
      // Fix the HTML ID for the Ajax callback.
      // @todo Append the entity ID in case of multiple forms on page.
      $element['#id'] = str_replace('-wrapper', '', $element/*[$langcode]*/['#county_id']);

      $admin_area_element = &$form[$admin_area_field_name];
      if (!empty($admin_area_element['#county_processed'])) {
        return; // continue;
      }
      $admin_area_element['#county_processed'] = 1;
      $langcode = empty($admin_area_element['#language']) ? LANGUAGE_NONE : $admin_area_element['#language'];

/*
      $parents = $form['#parents'];
      $ajax = array(
        // Only validate the administrative area field (the only dependent field).
        // This eventually makes it the only value in $form_state['values'].
//         '#limit_validation_errors' => array(array_merge($parents, array($admin_area_field_name, $langcode))),
        '#limit_validation_errors' => array(
          array_merge($parents, array($admin_area_field_name, $langcode)),
          // Do not include the county field; an error will occur if it is required
          // since it will have no value or an illegal value.
//           array_merge($parents, array($county_field_name, $langcode)),
        ),
        '#ajax' => array(
          'callback' => 'county_filter_widget_refresh', // 'county_widget_refresh',
          'effect' => 'fade', // @todo Add this to $settings in ajax_command_replace()?
          'event' => 'change',
        ),
      );
      $admin_area_element += $ajax;
*/

    }
  }

  /**
   * Returns admin_area field and its value.
   *
   * This value may come from a filter or an argument.
   *
   * @return array
   *   The admin_area field and its value.
   */
  function admin_area_settings($form, $form_state) {
    $admin_area_field = $admin_area_source = '';
    $admin_area = '_none';
    // Try the filter then the argument.
    // @todo Omit this filter from the list.
    if (!empty($this->options['admin_area_filter']) && $this->options['admin_area_filter'] != '_none') {
      $id = $this->options['admin_area_filter'];
      $filter_handler = $this->view->display_handler->get_handler('filter', $id);
      if (!empty($this->view->exposed_input[$id])) {
        $this->admin_area_source = 'filter';
        $this->admin_area_field = $id;
        $this->admin_area = $this->view->exposed_input[$id];
        return;
      }
      elseif (!empty($filter_handler->value)) {
        // The value property is not set at this point, unless set in custom code.
        $this->admin_area_source = 'filter';
        $this->admin_area_field = $id;
        $this->admin_area = current($filter_handler->value);
        return;
      }
    }
    if (!empty($this->options['admin_area_argument']) && $this->options['admin_area_argument'] != '_none') {
      $id = $this->options['admin_area_argument'];
      $argument_handler = $this->view->display_handler->get_handler('argument', $id);
      if (!empty($argument_handler) && !empty($argument_handler->get_value())) {
        $this->admin_area_source = 'argument';
        $this->admin_area_field = $id;
        $this->admin_area = $argument_handler->get_value();
        return;
      }
    }

    $this->admin_area_field = $admin_area_field;
    $this->admin_area = $admin_area;
    $this->admin_area_source = $admin_area_source;
  }

  /**
   * @todo
   * This is called to build settings form in views_ui and for exposed form.
   * For the former case, the handler has no value.
   */
  function get_value_options() {
    require_once __DIR__ . '/../../includes/edit.inc';
    $this->admin_area_settings([], []);
    // Views adds an option with an 'All' key only if one value is allowed on
    // the filter. This module adds an 'all' key that is always a valid choice.
    $this->value_options = county_widget_options($this->admin_area, ['display_type' => 'short', 'select_all' => TRUE]);
  }

  /**
   * Form validation handler for views_exposed_form().
   *
   * This could also be accomplished by JS to clear the county element on change
   * of the state element. Neither approach provides the desired solution to
   * refresh the county options on state change.
   *
   * @see views_exposed_form_validate()
   */
  function exposed_validate(&$form, &$form_state) {
    if (empty($form_state['county_field'])) {
      return;
    }

    // This assumes only one exposed form; is that every false?
    // Copied from county_widget_refresh().
    $parents_key = key($form_state['county_field']);
    $admin_area_field_name = key($form_state['county_field'][$parents_key]);
    foreach($form_state['county_field'][$parents_key][$admin_area_field_name] as $county_field_name => $langcode) {
      if (!empty($form[$county_field_name])) {
        $county_element = $form[$county_field_name];
      }
      else {
        // The county element is nested in form array; find the parent keys.
        $parents = explode('-', $parents_key);
        $field_state = field_form_get_state($parents, $county_field_name, $langcode, $form_state);
        $parents = $field_state['array_parents'];

        // Remove the langcode key.
        $parents = array_slice($parents, 0, -1);
        $county_element = drupal_array_get_nested_value($form, $parents, $key_exists);
        if (!$key_exists) {
          // @todo Do something more in this case.
          return [];
        }
      }
    }

    // @todo If element is nested then use $parents to access arrays below.
    $errors = &drupal_static('form_set_error', array());
    if (isset($errors[$county_field_name]) && strpos($errors[$county_field_name], 'An illegal choice has been detected') !== FALSE) {
      // This condition is met if:
      // - county values are selected
      // - state value is changed
      // - exposed filter form is submitted
      // because AJAX does not fire on state change to refresh the county options
      // as views uses a GET method (and core AJAX requiring a POST method).
      // Remove the form error and clear the county values so that the views
      // query omits a filter on county.
      // Remove error message.
      $key = array_search($errors[$county_field_name], $_SESSION['messages']['error']);
      unset($_SESSION['messages']['error'][$key]);
      if (empty($_SESSION['messages']['error'])) {
        unset($_SESSION['messages']['error']);
      }
      // Remove error.
      unset($errors[$county_field_name]);
      // Remove values from exposed input.
//       unset($form_state['view']->exposed_input[$county_field_name]);
//       unset($form_state['input'][$county_field_name]);
//       unset($form_state['values'][$county_field_name]);
//       $form_state['input'][$county_field_name] = [];
      $form_state['values'][$county_field_name] = [];
    }
  }
}
