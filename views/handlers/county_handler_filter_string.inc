<?php

/**
 * @file
 * Contains \county_handler_filter_string.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Filters on either the county FIPS value or county text.
 *
 * @todo Filter by FIPS value. Might also allow syntax like 'KS:Riley'.
 */
class county_handler_filter_string extends views_handler_filter_string {

  /**
   * Adds filter to the query.
   */
  function query__SAVE() {
    // @todo A join would be simpler if the county_fips table included a column
    // with same contents as the value column in a field table.
    $this->ensure_my_table();
    $field = "$this->table_alias.$this->real_field";
    $this->sub_select($field);
  }

  // Compare filter value to 'county' column in county_fips table.
  function sub_select($field) {
    $value = db_like($this->value);
    $sql = "$field IN (SELECT CONCAT(fips_state, fips_county) FROM county_fips WHERE county LIKE '%$value%')";
    $this->query->add_where_expression($this->options['group'], $sql);
  }



  function option_definition() {
    $options = parent::option_definition();
    $options['value_type'] = array('default' => 'fips');
    return $options;
  }

  /**
   * Form builder.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['value_type'] = array(
      '#type' => 'radios',
      '#title' => t('Value type'),
      '#description' => t('Indicate whether each filter value is text (Macon County) or FIPS (20161).'),
      '#default_value' => $this->options['value_type'],
      '#options' => ['text' => 'County text', 'fips' => 'FIPS value'],
    );
  }

  /**
   * Returns list of allowed operators.
   */
  function operators() {
    $operators = parent::operators();
    unset($operators['word'], $operators['allwords'], $operators['shorterthan'], $operators['longerthan'], $operators['regular_expression']);
    return $operators;
  }

  /**
   * Adds filter to the query.
   */
  function query() {
    if ($this->options['value_type'] == 'fips') {
      // This join will return a row for each delta row in the field table
      // whose value satisfies the filter criteria. If need be, add distinct
      // to the query settings of the view.
      parent::query();
      return;
    }

    $this->ensure_my_table();
    $field = "$this->table_alias.$this->real_field";

    // Join the county field table with the county_fips table.
    // The former has fips value; the latter has this split into two columns.
    $join = new views_join();
    $join->definition = array(
      'type'       => 'LEFT',
      'left_table' => '',
      'left_field' => "SUBSTR($field, 1, 2)",
      'table'      => 'county_fips',
      'field'      => 'fips_state',
      'extra'      => "SUBSTR($field, 3, 3) = county_fips.fips_county",
/*
      // This fails because 'value' becomes a placeholder surrounded by quotes.
      // Views lost a useful feature with removal of 'formula' key in extra.
      'extra'      => array(
        array(
          'table'      => 'county_fips',
          'field'      => 'fips_county',
          'value'      => "SUBSTR($field, 3, 3)",
        ),
      ),
*/
    );
    $join->construct();
    $join->adjusted = TRUE;
    $fips = $this->query->queue_table('county_fips', $this->relationship, $join/*, $alias*/);
    if ($fips != 'county_fips') {
      // Adjust table alias in extra join clause.
      $queue = &$this->query->table_queue;
      $queue[$fips]['join']->extra = str_replace('county_fips', $fips, $queue[$fips]['join']->extra);
    }

    $info = $this->operators();

    if (!empty($info[$this->operator]['method'])) {
      if ($info[$this->operator]['method'] != 'op_empty') {
        // Conditions involving NULL need to be applied against real_field; all
        // others against the fips county value.
        $field = "$fips.county";
      }
      $this->{$info[$this->operator]['method']}($field);
    }
  }

  // Each of these should compare filter value to 'county' column in county_fips table.
  // Use the relationship from the query above.
  function op_multiple($field) {
    $this->query->add_where($this->options['group'], $field, $this->value, $this->operator);
  }
}
