<?php

/**
 * @file
 * Provides views hook routines.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Implements hook_field_views_data_alter().
 */
function county_field_views_data_alter(&$data, $field, $module) {
  if ($module != 'county') {
    return;
  }

  $current_table = _field_sql_storage_tablename($field);
  $revision_table = _field_sql_storage_revision_tablename($field);
  $column_name = 'value';
  foreach ([$current_table, $revision_table] as $table) {
    $key = _field_sql_storage_columnname($field['field_name'], $column_name);
    if (isset($data[$table][$key])) {
      // Set custom filter handler using text element.
      $data[$table][$key]['filter']['handler'] = 'county_handler_filter_string';
      // Set custom filter handler using select element.
      $select_filter = $data[$table][$key];
      unset($select_filter['argument'], $select_filter['sort']);
      $select_filter['title'] .= ' (select element)';
      $select_filter['help'] .= ' The exposed filter uses a select element.';
      $select_filter['filter']['handler'] = 'county_handler_filter_many';
      $column_name = 'select';
      $key = _field_sql_storage_columnname($field['field_name'], $column_name);
      $data[$table][$key] = $select_filter;
    }
  }
}
